import logging
import sys
sys.path.insert(0, '..')
from message_queue.connect import connect_message_queue
from .handler import Handler as WeiboFanCrawler
from tornado import ioloop

mq = connect_message_queue("uid")

logging.basicConfig(
    level=logging.INFO
)

class Handler():

    def run_once(self):
        uid = mq.pop()
        if uid and uid != 'None':
            weibo_fan = WeiboFanCrawler(uid, mq)
            weibo_fan.run()
            mq.finish(uid)
        else:
            logging.info("get [uid: %s]" % uid)

    def run_forever(self):
        ioloop.PeriodicCallback(self.run_once, 1000).start()
        ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    handler = Handler()
    handler.run_for_ever()