import requests
from retry import retry

class Handler():
    '''
    for user info
    '''

    def __init__(self, uid=None):
        self.uid = uid

    '''
    @return(dict) user	
        id	2154491277
        screen_name	"小辫儿张云雷"
        profile_image_url	"https://tvax4.sinaimg.cn/crop.0.0.1125.1125.180/806aed8dly8fwb9j9aji1j20v90v9aen.jpg"
        profile_url	"https://m.weibo.cn/u/2154491277?uid=2154491277&luicode=20000174"
        statuses_count	452
        verified	true
        verified_type	0
        verified_type_ext	1
        verified_reason	"德云社相声演员、歌手"
        close_blue_v	false
        description	"工作以及邀约 请联系经纪人王俣钦先生： max.w@vip.163.com"
        gender	"m"
        mbtype	12
        urank	39
        mbrank	6
        follow_me	false
        following	false
        followers_count	5267164
        follow_count	98
        cover_image_phone	"https://wx2.sinaimg.cn/crop.0.0.640.640.640/806aed8dly1ff1lj69u6pj20e80e6abu.jpg"
        avatar_hd	"https://wx4.sinaimg.cn/orj480/806aed8dly8fwb9j9aji1j20v90v9aen.jpg"
        like	false
        like_me	false
    '''
    @retry(tries=3)
    def get(self, uid):
        url = 'https://m.weibo.cn/profile/info?uid=%s' % uid
        rsp = requests.get(url)
        return rsp.json()["data"]["user"]