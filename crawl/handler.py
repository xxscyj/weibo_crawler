import django
import json
import logging
import os
import requests
import sys
import time

sys.path.insert(0, '..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'weibo.settings'
django.setup()

from retry import retry

from model_app.models import User, FanRelation
from .user_info import Handler as UserAPi

user_api = UserAPi()

REQUEST_INTERVAL = 2
logging.basicConfig(
    level=logging.INFO
)
class Handler():
    '''
    for one user's fans
    '''

    '''
    @param uid(str): the user to be crawled
    @param max_count(int): the max fan count
    '''
    def __init__(self, uid=None, mq=None, max_count=300):
        if uid is None:
            raise RuntimeError("uid can not be None")
        self.uid = uid
        self.limit = 300
        self.counter = 0
        self.mq = mq

    '''
    check whethe the uid is in mysql
    '''
    def prepare_crawl(self):
        user = User.objects.filter(uid=self.uid)
        if len(user) == 0:
            logging.warning("the user is not in mysql, start to crawl user info")
            nuser = user_api.get(self.uid)
            deafults = self.extract_from_user(nuser)
            User.objects.update_or_create(
                uid=nuser.get("id"),
                deafults=deafults
            )
            logging.info("user_info finished")

    '''
    @param url(str): 
    '''
    @retry(tries=3)
    def crawl(self, url):
        rsp = requests.get(url)
        return rsp

    '''
    # extract info(exclude uid) from user
    @param user(dict): user get from response of a url
    '''
    def extract_from_user(self, user):
        defaults={
            "uname": user.get("screen_name", ''),
            "sex": user.get("gender", 'u') or 'u',
            "follow_count": user.get("follow_count", -1),
            "fan_count": user.get("followers_count", -1),
            "post_count": user.get("post_count", -1),
            "level": user.get("mrank", 0) or 0,
        }
        return defaults

    '''
    @param rsp: return value of requests
    '''
    def parse(self, rsp):
        logging.info("start parse page of [url: %s]" % rsp.request.url)
        j_rsp = rsp.json()
        cards = j_rsp["data"]["cards"]
        if len(cards) == 0:
            logging.info("has no more fans")
            return 0
        else:
            group = cards[-1]["card_group"]
            for card in group:
                user = card["user"]
                defaults = self.extract_from_user(user)
                logging.info(json.dumps(defaults))
                uid = user.get("id")
                if uid is None:
                    raise Exception("get user with no uid")
                else:
                    User.objects.update_or_create(
                        uid=uid,
                        defaults=defaults
                    )
                    FanRelation.objects.update_or_create(
                        fromid=self.uid,
                        toid=uid
                    )
                    self.counter += 1
                    self.mq.set_status(self.counter)
                    logging.info("mysql_saved current counter: %s" % self.counter)

    '''
    crawl users loop
    '''
    def run(self):
        logging.info("start run")
        url = 'https://m.weibo.cn/api/container/getIndex?containerid=231051_-_fans_-_{uid}&since_id={page}'
        page = 0
        while True:
            real_url = url.format(uid=self.uid, page=page)
            rsp = self.crawl(real_url)
            self.parse(rsp)
            if self.counter > self.limit:
                break
            time.sleep(REQUEST_INTERVAL)
            page += 1
            logging.info("after sleep %ss curr page %s" % (REQUEST_INTERVAL, page))

if __name__ == '__main__':
    handler = Handler('2154491277')
    handler.run()
            