import click
import sys
import threading
import logging
sys.path.insert(0, '..')

from message_queue.connect import connect_message_queue
from crawl.run import Handler
logging.basicConfig(
    level=logging.INFO
)

mq = connect_message_queue("uid")

@click.group()
def cli():
    pass


@cli.command()
@click.option("--uid")
@click.option("--filename")
@click.option("--thread_count", default=1, type=int)
def run(uid, filename, thread_count):
    if not(uid or filename):
        raise RuntimeError("uid or filename need")
    if uid is not None:
        uid_list = uid.split(',')
    elif filename is not None:
        with open(filename) as f:
            uid_list = f.readlines()
    for uid in uid_list:
        mq.push(uid) 
    if thread_count == 1:
        handler = Handler()
        handler.run_forever()
    else:
        ts = [threading.Thread(target=handler.run_forever) for i in range(thread_count)]
        for thread in ts:
            thread.start()
        for thread in ts:
            thread.join()

@cli.command()
def reset():
    mq.reset()

@cli.command()
def status():
    logging.info("current mq [size: %s, counter: %s]" % mq.get_status())

@cli.command()
@click.option("--export", help="input a filename")
def show_fail(export):
    fails = mq.get_fail()
    for fail in fails:
        logging.info("fail [uid: %s]" % (fail))
    if export:
        with open(export, "w") as f:
            for fail in fails:
                f.write(fail)



'''
example:
    python3 bootstrap.py --uid 1212323 --thread_count 1
    python3 bootstrap.py --filename uid.txt --thread_count 3
'''

if __name__ == '__main__':
    cli()
            