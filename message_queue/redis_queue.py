import datetime
import redis
from enum import Enum

class PushResult(Enum):
    NORMAL = 0
    EXISTED = 1

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

class RedisQueue():
    
    def __init__(self, name):
        self.name = name
        self.unfinish_set = name + '_set'
        self.queue_hset = name + '_queued'
        self.status = name + "_status"
        self.client = redis.StrictRedis(decode_responses=True)

    def get_fail(self):
        fails = self.client.smembers(self.unfinish_set)
        return fails

    def reset(self):
        self.client.delete(self.unfinish_set)
        self.client.delete(self.queue_hset)

    def pop(self):
        return str(self.client.lpop(self.name))

    '''
    check whether crawled
    '''
    def push(self, task, check=True):
        # import pdb; pdb.set_trace()
        curr_time = datetime.datetime.now()
        last_time = str(self.client.hget(self.queue_hset, task))
        if check and last_time and last_time != 'None':
            last_time = datetime.datetime.strptime(last_time, DATETIME_FORMAT)
            if curr_time - last_time < datetime.timedelta(days=1):
                return PushResult.EXISTED.value
        self.client.rpush(self.name, task)
        self.client.sadd(self.unfinish_set, task)
        return PushResult.NORMAL.value

    def size(self):
        return self.client.llen(self.name)

    def finish(self, task):
        curr_time = datetime.datetime.now()
        self.client.srem(self.name, task)    
        self.client.hset(self.queue_hset, task, str(curr_time).split('.')[0])

    def set_status(self, count):
        self.client.hset(self.status, "counter", count)

    def get_status(self):
        size = self.size()
        count = self.client.hget(self.status, "counter")
        if count is None or count == 'None':
            count = -1
        return (size, count)