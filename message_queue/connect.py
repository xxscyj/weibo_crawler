from .redis_queue import RedisQueue

def connect_message_queue(name):
    return RedisQueue(name)
    