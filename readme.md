1. 依赖 
- python 
    python -m pip install -r requirements.txt
- 服务
    redis
    
2. 修改weibo/settings.py 部分 

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'your db',
        'USER': 'your user',
        'PASSWORD': 'your_password'
    }
}
```

3.  在根目录

```python
python manage.py makemigrations
python manage.py migrate
```

4. 执行bootstrap.py
