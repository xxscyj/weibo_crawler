from django.db import models

# Create your models here.

class User(models.Model):
    uid = models.CharField(max_length=50, db_index=True)
    uname = models.CharField(max_length=100)
    sex = models.CharField(max_length=2)
    follow_count = models.IntegerField()
    fan_count = models.IntegerField()
    post_count = models.IntegerField()
    level = models.IntegerField(default=0)
    update_time = models.DateTimeField(auto_now=True)

class FanRelation(models.Model):
    fromid = models.CharField(max_length=50, db_index=True)
    toid = models.CharField(max_length=50, db_index=True)
    type=models.CharField(max_length=10, default="fan")
    update_time = models.DateTimeField(auto_now=True)

